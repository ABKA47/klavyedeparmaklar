import axios from "../../../axios";

import AuthHeader from '../../../containers/Auth/Services/AuthHeader'

class UploadFilesService {
  
  upload(projects, onUploadProgress) {

    let formData = new FormData();

    formData.append("projectName", projects.projectName);
    formData.append("projectContent", projects.projectContent);
    formData.append("projectData", projects.projectData);
   
    return axios.post("projects/addproject", formData, {
      headers: AuthHeader(),
      onUploadProgress,
    }
    );

  }

  uploadAbout(abouts) {
    let formData = new FormData();

    formData.append("aboutName", abouts.aboutName);
    formData.append("aboutTitle", abouts.aboutTitle);
    formData.append("aboutContent", abouts.aboutContent);
    formData.append("aboutData", abouts.aboutData);

    console.log("formData: ", formData)

    return axios.post("about/addabout", formData, {
      headers:
        AuthHeader()
    });
  }

}

export default new UploadFilesService();