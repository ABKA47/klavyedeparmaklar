import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import UploadService from './UploadFileService';
import * as action from '../../../store/actions/index';

class UploadFiles extends Component {
    constructor() {
        super();
        this.upload = this.upload.bind(this);

        this.state = {
            progress: 0,
            message: "",
        };
    }



    upload() {
        if ((this.props.projects.projectName.value === '' || this.props.projects.projectContent.data === '') && !this.props.projects.projectData.fileValue[0]) {
            return (
                alert("Alanları Doldurun!")
            )

        }
        else {
            if ((this.props.projects.projectName.value !== '' || this.props.projects.projectContent.data !== '') && !this.props.projects.projectData.fileValue[0]) {

                this.props.stateChanged();
                window.location.reload()
            }
            else {

                const projects = {
                    projectName: this.props.projects.projectName.value,
                    projectContent: this.props.projects.projectContent.data,
                    projectData: this.props.projects.projectData.fileValue[0]
                }

                const projectsList = {
                    projects: projects
                }

                console.log("projectsList : ", projectsList.projects)

                UploadService.upload(projectsList.projects, (event) => {
                    this.setState({
                        progress: Math.round((100 * event.loaded) / event.total),
                    });
                })
                    .catch(() => {
                        this.setState({
                            progress: 0,
                            message: "Could not upload the file!",
                            currentFile: undefined,
                        });
                    });
            }
        }
    }

    render() {
        const { progress, message, } = this.state;
        return (
            <div>
                <div className="progress">
                    <div
                        className="progress-bar progress-bar-info progress-bar-striped"
                        role="progressbar"
                        aria-valuenow={progress}
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style={{ width: progress + "%" }}
                    >
                        {progress}%
                  </div>
                </div>
                <button
                    className="btn btn-success"
                    onClick={this.upload} >Upload</button>

                <div className="alert alert-light" role="alert">
                    {message}
                </div>


            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onUpdateEditProject: (projectsData) => dispatch(action.updateStateEditProject(projectsData))


    }
}

export default withRouter(connect(null, mapDispatchToProps)(UploadFiles));