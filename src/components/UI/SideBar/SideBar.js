import React from 'react'

import { NavLink } from 'react-router-dom';
import classes from './SideBar.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'


const SideBar = (props) => {
    return (
        <aside className={classes.SidebarContainer}
            style={{
                top: props.show ? '0' : '-100%',
                opacity: props.show ? '1' : '0'
            }}>
            <div className={classes.SidebarIcon}>
                <FontAwesomeIcon icon={faTimes} onClick={props.closeSideBar}/>
            </div>
            <div className={classes.SidebarWrapper}>
                <ul className={classes.SidebarMenu}>
                    <NavLink to="/projects" className={classes.SidebarLink} onClick={props.closeSideBar} data-text="PROJECTS">PROJECTS</NavLink>
                    <NavLink to="/comingsoon" className={classes.SidebarLink} onClick={props.closeSideBar} data-text="COMING SOON">COMING SOON</NavLink>
                    <NavLink to="/about" className={classes.SidebarLink} onClick={props.closeSideBar} data-text="ABOUT">ABOUT</NavLink>
                    <NavLink to="/contact" className={classes.SidebarLink} onClick={props.closeSideBar} data-text="CONTACT">CONTACT</NavLink>
                </ul>
            </div>
        </aside>
    )
}

export default SideBar
