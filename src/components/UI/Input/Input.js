import React from 'react';

import { CKEditor } from '@ckeditor/ckeditor5-react';

import classes from './Input.css';

const input = (props) => {
    let inputElement = null;
    const inputClasses = [classes.InputElement];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);

    }

    switch (props.elementType) {
        case ('input'):
            inputElement = <input
                className={inputClasses.join(' ')}
                style={props.style}
                {...props.elementConfig}
                value={props.value}
                placeholder={props.placeholder}
                ref={props.ref}
                onChange={props.changed} />;
            break;
        case ('textarea'):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                style={props.style}
                value={props.value}
                placeholder={props.placeholder}
                onChange={props.changed} />;
            break;
        case ('select'):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    onChange={props.changed}
                    style={props.style}
                    value={props.value}
                    defaultValue={props.defaultValue}>
                    <option key={props.defaultValue} value={props.defaultValue} disabled>
                        {props.defaultValue}
                    </option>

                    {props.elementConfig.options.map(option => (

                        <option key={option.value} value={option.value}  >
                            {option.displayValue}
                        </option>

                    ))}
                </select>
            );
            break;
        case ('radio'):
            inputElement = (
                <input
                    className={inputClasses.join(' ')}
                    onChange={props.changed}
                    type={props.elementType}
                    value={props.value}>

                </input>
            )
            break;
        case ('CKEditor'):
            return (
                <CKEditor
                    editor={props.editor}
                    data={props.data}
                    onChange={props.editorChanged}
                />)


        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div className="Input">
            {inputElement}
        </div>
    );

};

export default input;
