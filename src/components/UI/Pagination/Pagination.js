import React from 'react'
import { NavLink } from 'react-router-dom'
import classes from './Pagination.css'

const Pagination = ({ contentPerPage, totalContent, paginate }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalContent / contentPerPage); i++) {
        pageNumbers.push(i);
    }
    return (
        <nav>
            <ul className={classes.Pagination}>
                {pageNumbers.map(number => (
                    <li key={number} className={classes.PageItem}>
                        <NavLink className={classes.PageLink} onClick={() => paginate(number)} to='/dashboard/editproject' exact >{number}</NavLink>
                    </li>
                ))}

            </ul>
        </nav>
    )
}
export default Pagination;