import React, { Component } from 'react';
import classes from '../styles/ComingSoon.css';

import CountDown from './CountDown';
import Logo from './Logo'
import { Title } from './Title';
import { Description } from './Description';
import Links from './Links'

import gitlab from '../images/gitlab.png'

class ComingSoon extends Component {
  state = {
    countDown: {
      futureDate: "2020-12-31 00:00:00"
    },
    title: {
      text: "Coming Soon!"
    },
    description: {
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo malesuada sapien sit amet ornare. Nullam in iaculis ipsum. Ut porttitor cursus dui et mollis. Pellentesque finibus felis fringilla ante feugiat auctor. Etiam dictum elementum ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam vestibulum at sem vel molestie. Proin viverra mauris vitae dolor commodo pellentesque. Nullam faucibus quam est, eu fermentum mauris fringilla sed. Praesent accumsan sem ut lacus auctor scelerisque sit amet sit amet sapien."
    },
    subscribe: {
      placeholder: "Enter Email Address",
      buttonText: "Submit"
    },
    links: {
      url: "https://gitlab.com/klavyedeparmaklar/coming-soon-page",
      logo: gitlab,
    }
  }

  render() {

    const { countDown, title, description, subscribe, links } = this.state;

    return (
      <div className={classes.Background}>
        <CountDown futureDate={countDown.futureDate} ></CountDown>
         <Logo />
        <Title text={title.text} />
        <Description text={description.text} />
        <Links linkurl={links.url} linklogo={links.logo} />
      </div>
    );
  }
}
export default ComingSoon;
