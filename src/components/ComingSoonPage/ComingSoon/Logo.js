import React, { Component } from 'react'

import classes from '../styles/Logo.css'
import logo from '../images/logo.svg'

export default class Logo extends Component {
    render() {
        return (
            <div className={classes.App}>
                <header>
                    <img src={logo} className={classes.AppLogo} alt="logo" />
                </header>
            </div>
        )
    }
}
