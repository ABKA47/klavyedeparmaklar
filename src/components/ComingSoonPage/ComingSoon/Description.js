import React from 'react';

import classes from '../styles/Description.css';

export const Description = ({ text }) => {
    return (
        <div className={classes.description}>
            <p className={classes.descriptionText}>{text}</p>
        </div>
    )
}
