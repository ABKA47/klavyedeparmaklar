import React from 'react';

import classes from '../styles/Title.css';

export const Title = ({ text }) => {
    return (
        <h1 className={classes.title}>{text}</h1>
    )
}
