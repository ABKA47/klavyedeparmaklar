import React, { Component } from 'react'

import classes from "../styles/Links.css";

class Links extends Component {
    render() {
        const { linkurl, linklogo } = this.props

        return (
            <div>
                <a target="_blank" href={linkurl}>
                    <img className={classes.socialLogo} src={linklogo} />
                </a>
            </div>
        )
    }
}

export default Links