import React from 'react'
import classes from './Home.css';

const home = () => {
    return (
        <div>
            <section className={classes.Showcase}>
                <div className={classes.Container, classes.Grid}>
                    <div className={classes.CardKP} >
                        <div className={classes.CardKPPhrase}>
                            <h2>PHRASE</h2>
                        </div>
                        <div className={classes.CardKPComponent}>
                            <h1 className={classes.CardKPItem}>KLAVYEDEPARMAKLAR</h1>
                            <p className={classes.CardKPItem}>the fingers on the keyboard.</p>
                        </div>
                    </div>
                    <div className={classes.Showcasetext}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nec posuere ante. In eleifend arcu quis hendrerit lacinia.
                            Aliquam erat volutpat. Cras ut enim diam. Suspendisse pulvinar feugiat leo, non.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nec posuere ante. In eleifend arcu quis hendrerit lacinia.
                            Aliquam erat volutpat. Cras ut enim diam. Suspendisse pulvinar feugiat leo, non.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nec posuere ante. In eleifend arcu quis hendrerit lacinia. Aliquam erat volutpat. Cras ut enim diam. Suspendisse pulvinar feugiat leo, non.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </p>
                    </div>
                </div>
            </section>
        </div>
    )
}
export default home;