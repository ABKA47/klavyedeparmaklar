import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import classes from './Navbar.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
const navbar = (props) => {

    if (props.token) {
        return (
            <nav className={classes.Navbar}>
                <ul className={classes.Menu}>
                    <li><NavLink to="/projects" className={classes.Projects}>PROJECTS</NavLink></li>
                    <li><NavLink to="/comingsoon" data-text="COMING SOON!!" className={classes.Cuming}>COMING SOON!!</NavLink></li>
                </ul>
                <NavLink to="/home" className={classes.Logo}>
                    <h1>KP</h1>
                </NavLink>
                <div className={classes.Icon} onClick={props.openSideBar}>
                    <FontAwesomeIcon icon={faBars} />

                </div>
                <ul className={classes.Menu}>
                    <li><NavLink to="/about" className={classes.Links}>ABOUT</NavLink></li>
                    <li><NavLink to="/dashboard" className={classes.Links}>DASHBOARD</NavLink></li>
                    <li><NavLink to="/contact" className={classes.Links}>CONTACT</NavLink></li>
                </ul>
            </nav>
        )
    }
    else {
        return (
            <nav className={classes.Navbar}>
                <ul className={classes.Menu}>
                    <li><NavLink to="/projects" className={classes.Projects}>PROJECTS</NavLink></li>
                    <li><NavLink to="/comingsoon" data-text="COMING SOON!!" className={classes.Cuming}>COMING SOON!!</NavLink></li>
                </ul>
                <NavLink to="/home" className={classes.Logo}>
                    <h1>KP</h1>
                </NavLink>
                <div className={classes.Icon} onClick={props.openSideBar}>
                    <FontAwesomeIcon icon={faBars} />

                </div>
                <ul className={classes.Menu}>
                    <li><NavLink to="/about" className={classes.Links}>ABOUT</NavLink></li>
                    <li><NavLink to="/contact" className={classes.Links}>CONTACT</NavLink></li>

                </ul>
            </nav>
        )

    }

}
export default navbar;