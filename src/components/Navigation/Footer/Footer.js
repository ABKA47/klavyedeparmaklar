import React from 'react';
import classes from './Footer.css'
import { FaGithub, FaLinkedin, FaGitlab } from 'react-icons/fa'

const Footer = () => {
    const today = new Date()

    return (
        <footer className={classes.Footer}>
            <div className={classes.Container, classes.Grid}>
                <div className={classes.Copyright}>
                    <p>
                        Copyright &copy; {today.getFullYear()}
                        <a className="text-reset text-decoration-none" href="" target="_blank">
                            <span> KlavyedeParmaklar </span>
                        </a>
                    all rights reserved
                    </p>
                </div>
                <div className={classes.Social}>
                    <a><FaGithub  /></a>
                    <a><FaGitlab  /></a>
                    <a><FaLinkedin  /></a>
                </div>
            </div>
        </footer>
    );
}


export default Footer;
