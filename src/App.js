import React, { Component } from 'react'
import classes from './App.css';
import { connect } from 'react-redux';
import * as action from './store/actions/index';

import { Switch, Route, Redirect } from 'react-router-dom'
import Navbar from './components/Navigation/Navbar/Navbar';
import Footer from './components/Navigation/Footer/Footer'
import Home from './components/Home/Home';
import ComingSoon from './components/ComingSoonPage/ComingSoon/ComingSoon';
import Projects from './containers/Projects/Projects';
import About from './containers/About/About';
import Contact from './containers/Contact/Contact';
import Login from './containers/Auth/Login/Login';
import Dashboard from './containers/Dashboard/Dashboard';
import EditProject from './containers/Dashboard/EditProject/EditProject'
import EditAbout from './containers/Dashboard/EditAbout/EditAbout';
import SideBar from './components/UI/SideBar/SideBar'
import Register from './containers/Auth/Register/Register';

class App extends Component {
  state = {
    show: false
  }
  openSideBar = () => {
    this.setState({ show: true })
  }
  closeSideBar = () => {
    this.setState({ show: false })
  }
  render() {
    let token = JSON.parse(localStorage.getItem('user'));
    let route;
    if (token) {
      setInterval(() => {
        this.props.onExpiration(token.expiresIn)
      }, token.expiresIn)
      route = (
        < div >
          <Switch>
            <Route exact path="/home" component={Home} />
            <Route path="/projects" component={Projects} />
            <Route path="/comingsoon" component={ComingSoon} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/dashboard" component={Dashboard} />
            <Redirect to="/home" />
          </Switch>
        </div >
      )
    }

    else {
      route = (
        <Switch>
          <Route exact path="/home" component={Home} />
          <Route path="/projects" component={Projects} />
          <Route path="/comingsoon" component={ComingSoon} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/theBoss" component={Login} />
          <Redirect to="/home" />
        </Switch>
      )
    }

    return (
      <div className={classes.App}>
        <div className={classes.AppMenu}>
          <SideBar show={this.state.show} closeSideBar={this.closeSideBar} />
          <Navbar openSideBar={this.openSideBar} token={token} />
        </div>

        <div className={classes.AppRoute} >
          {route}
        </div>
        <div className={classes.AppFooter} >
          <Footer />
        </div>
      </div>
    )
  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    onRefreshToken: (refreshToken, expiresIn) => dispatch(action.getRefreshToken(refreshToken, expiresIn)),
    onExpiration: (miliseconds) => dispatch(action.renewAccessTokenIfGoingExpire(miliseconds))

  }
}
export default connect(null, mapDispatchToProps)(App);