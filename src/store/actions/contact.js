import * as actionTypes from './actionTypes';

export const updateContactState = (updatedContact, formIsValid) => {
    return {
        type: actionTypes.UPDATECONTACTSTATE,
        updatedContact: updatedContact,
        formIsValid: formIsValid
    }
}