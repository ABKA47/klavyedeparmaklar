import * as actionTypes from './actionTypes';
import axios from '../../axios';
import AuthHeader from '../../containers/Auth/Services/AuthHeader';

export const updateStateProjects = (updatedProjectData) => {
    return {
        type: actionTypes.UPDATESTATEPROJECTS,
        data: updatedProjectData
    }
}
export const updateStateEditProject = (projectData) => {
    return {
        type: actionTypes.UPDATESTATEEDITPROJECT,
        projectData: projectData
    }
}
export const editProject = (projectId) => {
    return {
        type: actionTypes.EDITPROJECT,
        projectId: projectId
    }
}
export const closeEditProjectModal = () => {
    return {
        type: actionTypes.CLOSEEDITPROJECTMODAL
    }
}
export const SetCurrentPage = (pageNumber) => {
    return {
        type: actionTypes.SETCURRENTPAGE,
        pageNumber: pageNumber
    }
}
export const updateStateEditProjectModal = (editProjectModalState) => {
    return {
        type: actionTypes.UPDATESTATEEDITPROJECTMODAL,
        editProjectModalState: editProjectModalState
    }
}
export const openProjectsModal = (projectId) => {
    return {
        type: actionTypes.OPENPROJECTSMODAL,
        projectId: projectId
    }
}
export const closeProjectsModal = () => {
    return {
        type: actionTypes.CLOSEPROJECTSMODAL
    }
}
export const deleteProject = (projectId) => {
    return dispatch => {
        axios.post("projects/deleteproject", { projectId }).then(response => {
            window.location.reload()
        })
    }
}
export const fetchProjects = () => {
    return dispatch => {
        const API_URL_PROJECTS = 'projects/getproject'
        axios.get(API_URL_PROJECTS).then(response => {
            dispatch(updateStateProjects(response.data))
        })
    }
}

export const addProjects = (project) => {
    return dispatch => {
        const API_URL_PROJECTS = 'projects/addproject'
        axios.post(API_URL_PROJECTS, project, { headers: AuthHeader() })
    }
}

