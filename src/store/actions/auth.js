import axios from '../../axios';
import * as actionTypes from './actionTypes';
import AuthService from '../../containers/Auth/Services/Auth.service';
import UserService from '../../containers/Auth/Services/User.service';
import jwtDecode from 'jwt-decode';
import moment from 'moment';

export const authStart = () => {
    return {
        type: actionTypes.AUTHSTART
    }
}
export const updateLoginState = (updatedLogin) => {
    return {
        type: actionTypes.UPDATELOGINSTATE,
        updatedLogin: updatedLogin,

    }
}
export const authRefresh = (accessToken, refreshToken, expirationTime) => {
    return {
        type: actionTypes.AUTHREFRESH,
        accessToken: accessToken,
        refreshToken: refreshToken,
        expTime: expirationTime
    }
}

export const authLogin = () => {
    return {
        type: actionTypes.AUTHLOGIN,

    }
}
export const updatedRegisterState = (updatedRegister, formIsValid) => {
    return {
        type: actionTypes.UPDATEREGISTERSTATE,
        updatedRegister: updatedRegister,
        formIsValid: formIsValid
    }
}

export const fetchUsers = (usersData) => {
    return {
        type: actionTypes.FETCHUSERS,
        usersData: usersData
    }
}
export const editUser = (id) => {
    return {
        type: actionTypes.EDITUSER,
        id: id
    }
}
export const editUserState = (editUserData, formIsValid) => {
    return {
        type: actionTypes.EDITUSERSTATE,
        editUserData: editUserData,
        formIsValid: formIsValid
    }
}

export const closeModal = () => {
    return {
        type: actionTypes.CLOSEMODAL
    }
}
export const fetchUsersData = () => {
    return dispatch => {
        UserService.getAllUsers().then(response => {
            dispatch(fetchUsers(response.data))
        })
    }
}
export const authenticationLogin = (userPassword, userName) => {
    return dispatch => {
        dispatch(authStart())
        AuthService.login(userPassword, userName).then(response => {

            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
                dispatch(authLogin())
                window.location.reload();
            }
        }
        );
    }
}

export const getRefreshToken = (refreshToken) => {
    return dispatch => {
        let API_URL_REFRESHTOKEN = "auth/refreshToken";

        axios.post(API_URL_REFRESHTOKEN, { refreshToken })
            .then(response => {

                if (response.data.accessToken) {

                    localStorage.setItem("user", JSON.stringify(response.data))

                }
            })


    }
}

export const checkAccessTokenWillExpireInDay = (miliseconds) => {
    // decode access token
    // check exp
    // if will exp in a day
    return dispatch => {
        let token = JSON.parse(localStorage.getItem('user'));
        let decoded = jwtDecode(token.accessToken);
        moment(decoded.exp * 1000).subtract(miliseconds, 'milliseconds').isBefore(moment());
    };
}
export const renewAccessTokenIfGoingExpire = (miliseconds) => {
    return dispatch => {
        let willExpire = checkAccessTokenWillExpireInDay(miliseconds);
        console.log("willexpire", willExpire)
        if (willExpire) {
            let token = JSON.parse(localStorage.getItem('user'));
            dispatch(getRefreshToken(token.refreshToken))

        }
    }

};

export const deleteUser = (userId) => {
    return dispatch => {
        axios.post("auth/deleteuser", { userId }).then(response=>{
            window.location.reload()
        })
    }
}