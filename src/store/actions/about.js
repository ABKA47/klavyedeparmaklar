import * as actionTypes from './actionTypes';
import AuthHeader from '../../containers/Auth/Services/AuthHeader';
import axios from '../../axios';

export const updateStateEditAbout = (updateEditAbout) => {
    return {
        type: actionTypes.UPDATESTATEEDITABOUT,
        data: updateEditAbout
    }
}
export const updateStateEditAboutModal = (updatedEditAboutModal) => {
    return {
        type: actionTypes.UPDATESTATEEDITABOUTMODAL,
        updatedEditAboutModal: updatedEditAboutModal
    }
}
export const editAbout = (aboutId) => {
    return {
        type: actionTypes.EDITABOUT,
        aboutId: aboutId
    }
}
export const closeEditAboutModal = () => {
    return {
        type: actionTypes.CLOSEEDITABOUTMODAL,
    }
}
export const updateStateAbout = (updateAbout) => {
    return {
        type: actionTypes.UPDATESTATEABOUT,
        data: updateAbout
    }
}

export const fetchAbout = () => {
    return dispatch => {
        const url = 'about/getabouts'
        axios.get(url).then(response => {
            dispatch(updateStateAbout(response.data))

        })

    }
}

export const addAbout = (formData) => {
    return dispatch => {
        const url = 'about/addabout'
        axios.post(url, formData, { headers: AuthHeader() })
    }
}

export const deleteAbout = (aboutId) => {
    return dispatch => {
        axios.post("about/deleteabout", { aboutId }).then(response => {
            window.location.reload()
        })
    }
}