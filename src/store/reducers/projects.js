import * as actionTypes from '../actions/actionTypes';
import { updatedObject } from '../utility';

const initialState = {
    addProjects: {
        projectName: {
            type: 'input',
            elementConfig: {
                type: 'text',
            },
            value: '',
            placeholder: 'Project Name'
        },
        projectContent: {
            type: 'CKEditor',
            elementConfig: {
                type: 'text'
            },
            value: '',
            data: '',
            placeholder: 'Type the content here!',
        },
        projectData: {
            type: 'input',
            elementConfig: {
                type: 'file'
            },
            fileValue: ''
        }
    },
    editProjects: {
        projectName: {
            type: 'input',
            elementConfig: {
                type: 'text',
            },
            value: '',
            placeholder: 'Project Name'
        },
        projectContent: {
            type: 'CKEditor',
            elementConfig: {
                type: 'text'
            },
            value: '',
            data: '',
            placeholder: 'Write somethings about project...',

        },
        projectData: {
            type: 'input',
            elementConfig: {
                type: 'file'
            },
            fileValue: ''
        }
    },
    projects: [],
    projectList: [],
    projectsModalList: [],
    show: false,
    editable: false,
    currentPage: 1,
    contentPerPage: 4
}

const updateStateProject = (state, action) => {
    return updatedObject(state, { projects: action.data })
}
const updateStateEditProject = (state, action) => {
    return updatedObject(state, { addProjects: action.projectData })
}
const editProject = (state, action) => {
    let project = state.projects.filter(project => project.projectId === action.projectId)
    state.editProjects.projectName.value = project[0].projectName
    state.editProjects.projectContent.data = project[0].projectContent
    state.editProjects.projectData.fileValue = project[0].projectData
    return updatedObject(state, { projectList: project, editable: true })
}
const closeEditProjectModal = (state, action) => {
    return updatedObject(state, { editable: false })
}
const updateStateEditProjectModal = (state, action) => {
    return updatedObject(state, { editProjects: action.editProjectModalState })
}
const setCurrentPage = (state, action) => {
    return updatedObject(state, { currentPage: action.pageNumber })
}
const openProjectsModal = (state, action) => {
    let modalProject = state.projects.filter(project => project.projectId === action.projectId)
    return updatedObject(state, { projectsModalList: modalProject, show: true })
}
const closeProjectsModal = (state, action) => {
    return updatedObject(state, { show: false })
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATESTATEPROJECTS:
            return updateStateProject(state, action)
        case actionTypes.UPDATESTATEEDITPROJECT:
            return updateStateEditProject(state, action)
        case actionTypes.EDITPROJECT:
            return editProject(state, action)
        case actionTypes.CLOSEEDITPROJECTMODAL:
            return closeEditProjectModal(state, action)
        case actionTypes.UPDATESTATEEDITPROJECTMODAL:
            return updateStateEditProjectModal(state, action)
        case actionTypes.SETCURRENTPAGE:
            return setCurrentPage(state, action)
        case actionTypes.OPENPROJECTSMODAL:
            return openProjectsModal(state, action)
        case actionTypes.CLOSEPROJECTSMODAL:
            return closeProjectsModal(state, action)
        default:
            return state;
    }
}
export default reducer;