import * as actionTypes from '../actions/actionTypes';
import { updatedObject } from '../utility';

const initialState = {
    addAbout: {
        aboutName: {
            type: 'input',
            elementConfig: {
                type: 'text',
            },
            value: '',
            placeholder: 'Name'
        },
        aboutTitle: {
            type: 'input',
            elementConfig: {
                type: 'text'
            },
            value: '',
            placeholder: 'Title'
        },
        aboutContent: {
            type: 'input',
            elementConfig: {
                type: 'text'
            },
            value: '',
            placeholder: 'Link'
        },
        aboutData: {
            type: 'input',
            elementConfig: {
                type: 'file'
            },
            fileValue: ''
        }
    },
    editAbout: {
        aboutName: {
            type: 'input',
            elementConfig: {
                type: 'text',
            },
            value: '',
            placeholder: 'Name'
        },
        aboutTitle: {
            type: 'input',
            elementConfig: {
                type: 'text'
            },
            value: '',
            placeholder: 'Title'
        },
        aboutContent: {
            type: 'input',
            elementConfig: {
                type: 'text'
            },
            value: '',
            placeholder: 'Link'
        },
        aboutData: {
            type: 'input',
            elementConfig: {
                type: 'file'
            },
            fileValue: ''
        }
    },
    abouts: [],
    editAbouts: [],
    editable: false
}

const updateStateEditAbout = (state, action) => {
    return updatedObject(state, { addAbout: action.data })
}
const updateStateEditAboutModal = (state, action) => {
    return updatedObject(state, { editAbout: action.updatedEditAboutModal })
}
const updateStateAbout = (state, action) => {
    return updatedObject(state, { abouts: action.data })
}
const editAbout = (state, action) => {
    let about = state.abouts.filter(about => about.aboutId === action.aboutId)

    state.editAbout.aboutName.value = about[0].aboutName
    state.editAbout.aboutTitle.value = about[0].aboutTitle
    state.editAbout.aboutContent.value = about[0].aboutContent

    return updatedObject(state, {
        editAbouts: about,
        editable: true
    })
}
const closeModal = (state, action) => {
    return updatedObject(state, {
        editable: false
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATESTATEEDITABOUT: return updateStateEditAbout(state, action);
        case actionTypes.UPDATESTATEABOUT: return updateStateAbout(state, action);
        case actionTypes.EDITABOUT: return editAbout(state, action)
        case actionTypes.CLOSEEDITABOUTMODAL: return closeModal(state, action)
        case actionTypes.UPDATESTATEEDITABOUTMODAL: return updateStateEditAboutModal(state, action)
        default: return state
    }
}

export default reducer