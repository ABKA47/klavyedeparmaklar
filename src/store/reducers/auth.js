import * as actionsTypes from '../actions/actionTypes';
import { updatedObject } from '../utility';

const initialState = {
    login: {
        password: {
            type: 'input',
            elementConfig: {
                type: 'password',
            },
            value: '',
        },
        userName: {
            type: 'input',
            elementConfig: {
                type: 'password'
            },
            value: '',
        }
    },
    register: {
        userName: {
            type: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'USERNAME'
            },
            value: '',
            valid: false,
            touched: false,
            validation: {
                required: true
            }
        },

        password: {
            type: 'input',
            elementConfig: {
                type: 'password',
                placeholder: 'PASSWORD'
            },
            value: '',
            valid: false,
            touched: false,
            validation: {
                required: true
            }
        },
        role: {
            type: 'select',
            elementConfig: {
                options: [
                    { value: 'ROLE_THEBOSS', displayValue: 'THE BOSS' },
                    { value: 'ROLE_DEVELOPER', displayValue: 'DEVELOPER' },
                    { value: 'ROLE_USER', displayValue: 'USER' }

                ]

            },
            value: 'ROLE_USER',
            validation: {},
            valid: true
        }

    },
    editUsers: {
        userName: {
            type: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'USERNAME'
            },
            value: '',
            valid: false,
            touched: false,
            validation: {
                required: true
            }
        },
        password: {
            type: 'input',
            elementConfig: {
                type: 'password',
                placeholder: 'PASSWORD'
            },
            value: '',
            valid: false,
            touched: false,
            validation: {
                required: true
            }
        },
        role: {
            type: 'select',
            elementConfig: {
                options: [
                    { value: 'ROLE_THEBOSS', displayValue: 'THE BOSS' },
                    { value: 'ROLE_DEVELOPER', displayValue: 'DEVELOPER' },
                    { value: 'ROLE_USER', displayValue: 'USER' }

                ]

            },
            value: 'ROLE_USER',
            validation: {},
            valid: true
        }
    },
    users: [],
    editUserList: [],
    editTable: false,
    token: '',
    refreshToken: '',
    expirationTime: '',
    loading: false,
    loginFormIsValid: false,
    registerFormIsValid: false,
    editUsersFormIsValid: false
}

//Login
const authStart = (state, action) => {
    return updatedObject(state, { loading: true });
}
const updatedLoginState = (state, action) => {

    return updatedObject(state, { login: action.updatedLogin })
}
const authLogin = (state, action) => {

    return updatedObject(state, {
        loading: false
    })
}

//Register
const updatedRegisterState = (state, action) => {
    state.registerFormIsValid = action.formIsValid
    return updatedObject(state, { register: action.updatedRegister })
}

const fetchUsers = (state, action) => {
    return updatedObject(state, { users: action.usersData })
}
const editUser = (state, action) => {
    let user = state.users.filter(user => user.userId === action.id)
    console.log("user : ", user)
    state.editUsers.userName.value = user[0].userName
    state.editUsers.password.value = user[0].userPassword
    state.editUsers.role.value = user[0].roles[0].name
    console.log("editusers: ", state.editUsers)
    return updatedObject(state, {
        editUserList: user,
        editTable: true
    })
}
const closeModal = (state, action) => {
    return updatedObject(state, {
        editTable: false
    })
}
const editUserState = (state, action) => {
    state.editUsersFormIsValid = action.formIsValid
    return updatedObject(state, {
        editUsers: action.editUserData
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionsTypes.AUTHSTART:
            return authStart(state, action);
        case actionsTypes.UPDATELOGINSTATE:
            return updatedLoginState(state, action)
        case actionsTypes.UPDATEREGISTERSTATE:
            return updatedRegisterState(state, action)
        case actionsTypes.AUTHLOGIN:
            return authLogin(state, action)
        case actionsTypes.FETCHUSERS:
            return fetchUsers(state, action)
        case actionsTypes.EDITUSER:
            return editUser(state, action)
        case actionsTypes.CLOSEMODAL:
            return closeModal(state, action)
        case actionsTypes.EDITUSERSTATE:
            return editUserState(state, action)
        default:
            return state;
    }
}

export default reducer;