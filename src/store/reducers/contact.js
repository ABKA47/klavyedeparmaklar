import * as actionsTypes from '../actions/actionTypes';
import { updatedObject } from '../utility';

const initialState = {
    contact: {
        name: {
            type: 'input',
            elementConfig: {
                type: 'text',
                
            },
            value: '',
            displayValue:'Your Name',
            valid: false,
            touched: false,
            validation: {
                required: true,
                isLetter: true,
            }
        },
        email: {
            type: 'input',
            elementConfig: {
                type: 'text',
               
            },
            value: '',
            displayValue:'Your E-Mail',
            valid: false,
            touched: false,
            validation: {
                required: true,
                isMail: true,
            }
        },
        subject: {
            type: 'input',
            elementConfig: {
                type: 'text',
               
            },
            value: '',
            displayValue:'Subject',
            valid: false,
            touched: false,
            validation: {
                required: true,

            }

        },
        message: {
            type: 'textarea',
            elementConfig: {
                type: 'text',
                
            },
            value: '',
            displayValue:'Your Message',
            valid: false,
            touched: false,
            validation: {
                required: true,

            }

        }
    },
    formIsValid: false
}

const updatedContactState = (state, action) => {
    state.formIsValid = action.formIsValid
    return updatedObject(state, { contact: action.updatedContact })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionsTypes.UPDATECONTACTSTATE:
            return updatedContactState(state, action)

        default:
            return state;
    }
}

export default reducer;