import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import contactReducer from './store/reducers/contact';
import authReducer from './store/reducers/auth';
import projectsReducer from './store/reducers/projects';
import aboutReducer from './store/reducers/about';


const rootReducer = combineReducers({
  contact: contactReducer,
  auth: authReducer,
  projects: projectsReducer,
  about: aboutReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk))

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <React.StrictMode>
        <App />
       
      </React.StrictMode>
    </BrowserRouter>
  </Provider>
);
ReactDOM.render(app, document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
