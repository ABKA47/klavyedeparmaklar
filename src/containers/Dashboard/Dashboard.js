import React from 'react'

import DashboardRoutes from './DashboardRoutes';
import DashboardSidebar from './Sidebar/DashboardSidebar';
import EditUser from './EditUser/EditUser'
import classes from './Dashboard.css';

const Dashboard = () => {
    return (
        <div className={classes.Dashboard}>
            <DashboardSidebar />
            <DashboardRoutes />            
        </div>
    )
}
export default Dashboard;