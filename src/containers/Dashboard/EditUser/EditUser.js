import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as action from '../../../store/actions/index';
import Input from '../../../components/UI/Input/Input'
import AuthService from '../../Auth/Services/Auth.service';
import classes from './EditUser.css'

class EditUser extends Component {

    checkValidity(value, rules) {
        let isValid = true
        if (!rules) {
            return true;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.isMail) {
            const pattern = /^.+@.+$/;
            isValid = pattern.test(value) && isValid
        }
        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedEditUser = { ...this.props.editUsers }
        const updatedState = { ...updatedEditUser[inputIdentifier] }

        updatedState.value = event.target.value
        //updatedState.valid = this.checkValidity(updatedState.value, updatedState.validation)
        updatedState.touched = true
        updatedEditUser[inputIdentifier] = updatedState
        let formIsValid = true
        // for (let inputIdentifier in updatedEditUser) {
        //     formIsValid = updatedEditUser[inputIdentifier].valid && formIsValid
        // }
        this.props.onEditUserState(updatedEditUser, formIsValid)
    }

    formSubmit = (event) => {
        event.preventDefault()
        const editUserData = {}
        for (let id in this.props.editUsers) {
            editUserData[id] = this.props.editUsers[id].value
        }
        const editUsersList = {
            editUserData: editUserData
        }
    
        AuthService.editUser(this.props.userList[0].userId, editUsersList.editUserData.userName, editUsersList.editUserData.password, editUsersList.editUserData.role).then(response => {
            console.log("messages:", response.data.message)
            window.location.reload()
        }, error => {
            console.log("Error: ", error.response)
        })
    }
    render() {
        const userList = this.props.user.map(user => {
            let editUsersArray = []

            for (let key in this.props.editUsers) {
                editUsersArray.push({
                    id: key,
                    config: this.props.editUsers[key]
                })
            }
            console.log("editUsers ", editUsersArray)

            return (

                <div className={classes.RegisterContainer}>
                    <br />
                    <form onSubmit={this.formSubmit} className={classes.EditRegisterForm, classes.Card}>
                        <h3>EDIT USER</h3>
                        {editUsersArray.map(inputElement => (
                            <div className={classes.EditRegister}>
                                <Input
                                    key={inputElement.id}
                                    elementType={inputElement.config.type}
                                    elementConfig={inputElement.config.elementConfig}
                                    placeholder={inputElement.config.elementConfig.placeholder}
                                    value={inputElement.config.value}
                                    invalid={!inputElement.config.validation}
                                    shouldValidate={inputElement.config.validation}
                                    touched={inputElement.config.touched}
                                    changed={(event) => this.inputChangedHandler(event, inputElement.id)}
                                />
                            </div>
                        ))
                        }
                        <button disabled={!this.props.formIsValid} className={classes.RegisterBtn}>UPDATE USER</button>
                    </form>
                    <br />
                </div>

            )
        })
        return (
            <div>
                {userList}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        editUsers: state.auth.editUsers,
        formIsValid: state.auth.editUsersFormIsValid,
        userList: state.auth.editUserList
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onEditUserState: (editUserData, formIsValid) => dispatch(action.editUserState(editUserData, formIsValid))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUser)