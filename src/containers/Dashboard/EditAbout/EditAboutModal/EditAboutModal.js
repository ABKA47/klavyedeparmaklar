import React, { Component } from 'react'
import { connect } from 'react-redux'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Input from '../../../../components/UI/Input/Input'
import classes from './EditAboutModal.css'
import DashboardService from '../../services/Dashboard.services'
import * as action from '../../../../store/actions/index'
class EditAboutModal extends Component {
   
    inputChangedHandler = (event, inputIdentifier) => {
        const updatedAbout = { ...this.props.editAbout };
        const updatedState = { ...updatedAbout[inputIdentifier] };
        updatedState.value = event.target.value;
        updatedState.fileValue = event.target.files
        updatedAbout[inputIdentifier] = updatedState
        this.props.onUpdateEditAboutState(updatedAbout);
    }

    formSubmit = (event) => {
        event.preventDefault()
        // const editAboutList = {
        //     aboutName: this.props.editAbout.aboutName.value,
        //     aboutTitle: this.props.editAbout.aboutTitle.value,
        //     aboutContent: this.props.editAbout.aboutContent.data,
        // }
        // let formData = new FormData();

        // formData.append("aboutName", editAboutList.aboutName.aboutName)
        // formData.append("aboutTitle", editAboutList.aboutTitle.aboutTitle)
        // formData.append("aboutContent", editAboutList.aboutContent.aboutContent)
        const editAbout = {
            
            aboutId: this.props.aboutList[0].aboutId,
            aboutName: this.props.editAbout.aboutName.value,
            aboutTitle: this.props.editAbout.aboutTitle.value,
            aboutContent: this.props.editAbout.aboutContent.value,
            aboutData: this.props.editAbout.aboutData.fileValue[0]
        }

        const editAboutList = {
            editAbout: editAbout
        }

        console.log("formdata : ", editAboutList)
        DashboardService.editAbout(editAboutList.editAbout)
    }
    render() {
        const aboutList = this.props.editAbouts.map((user, key) => {
            let editAboutArray = []

            for (let key in this.props.editAbout) {
                editAboutArray.push({
                    id: key,
                    config: this.props.editAbout[key]
                })
            }
            console.log("editUsers ", editAboutArray)
            return (
                <div className={classes.RegisterContainer} key={key}>
                    <br />
                    <form onSubmit={this.formSubmit} className={classes.EditRegisterForm, classes.Card}>
                        <h3>EDIT ABOUT CARD</h3>
                        {editAboutArray.map(inputElement => (
                            <div className={classes.EditRegister} key={inputElement.id}>
                                <Input
                                    key={inputElement.id}
                                    elementType={inputElement.config.type}
                                    elementConfig={inputElement.config.elementConfig}
                                    placeholder={inputElement.config.elementConfig.placeholder}
                                    value={inputElement.config.value}
                                    invalid={!inputElement.config.validation}
                                    shouldValidate={inputElement.config.validation}
                                    touched={inputElement.config.touched}
                                    changed={(event) => this.inputChangedHandler(event, inputElement.id)}
                                />
                            </div>
                        ))
                        }
                        <button className={classes.RegisterBtn}>UPDATE ABOUT CARD</button>
                    </form>
                    <br />
                </div>

            )
        })
        return (
            <div>
                {aboutList}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        editAbout: state.about.editAbout,
        aboutList: state.about.editAbouts
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onUpdateEditAboutState: (updatedEditAboutState) => dispatch(action.updateStateEditAboutModal(updatedEditAboutState))

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditAboutModal)