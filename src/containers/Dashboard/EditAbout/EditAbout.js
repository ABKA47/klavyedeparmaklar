import React, { Component, useState } from 'react'
import { connect } from 'react-redux'
import * as action from '../../../store/actions/index'
import classes from './EditAbout.css'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Input from '../../../components/UI/Input/Input'
import Modal from '../../../components/UI/Modal/Modal'
import EditAboutModal from './EditAboutModal/EditAboutModal'
import UploadService from '../../../components/UI/UploadFile/UploadFileService'
import * as AiIcons from 'react-icons/ai'

class EditAbout extends Component {
    componentDidMount() {
        this.props.onFetchAbout()
    }
    upload = () => {
        if ((this.props.abouts.aboutName.value !== '' || this.props.abouts.aboutContent.value !== '') && !this.props.abouts.aboutData.fileValue) {
            this.submitForm()
        }
        else {

            const abouts = {
                aboutName: this.props.abouts.aboutName.value,
                aboutTitle: this.props.abouts.aboutTitle.value,
                aboutContent: this.props.abouts.aboutContent.value,
                aboutData: this.props.abouts.aboutData.fileValue[0]
            }

            const aboutsList = {
                abouts: abouts
            }

            UploadService.uploadAbout(aboutsList.abouts)
        }
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedAbout = { ...this.props.abouts };
        const updatedState = { ...updatedAbout[inputIdentifier] };
        updatedState.value = event.target.value;
        updatedState.fileValue = event.target.files
        updatedAbout[inputIdentifier] = updatedState
        this.props.onUpdateEditAboutState(updatedAbout);
    }

    submitForm = (event) => {
        event.preventDefault();
        const aboutList = {
            aboutName: this.props.abouts.aboutName.value,
            aboutTitle: this.props.abouts.aboutTitle.value,
            aboutContent: this.props.abouts.aboutContent.value,
        }
        let formData = new FormData();

        formData.append("aboutName", aboutList.aboutName.aboutName)
        formData.append("aboutTitle", aboutList.aboutTitle.aboutTitle)
        formData.append("aboutContent", aboutList.aboutContent.aboutContent)

        console.log("formdata : ", formData)
        this.props.onAddAbout(formData);
    }
    render() {
        let aboutArray = [];
        for (let key in this.props.abouts) {
            aboutArray.push({
                id: key,
                config: this.props.abouts[key]
            })
        }

        let form = (
            <form className={classes.EditAboutForm, classes.Card}>
                <h3>Edit About Page</h3>
                {aboutArray.map(aboutElement => (
                    <div className={classes.EditAbout} key={aboutElement.id}>
                        <Input
                            key={aboutElement.id}
                            elementType={aboutElement.config.type}
                            elementConfig={aboutElement.config.elementConfig}
                            value={aboutElement.config.value}
                            placeholder={aboutElement.config.placeholder}
                            changed={(event) => this.inputChangedHandler(event, aboutElement.id)}
                        />
                    </div>
                ))}
                <button className={classes.AboutBtn} onClick={this.upload}>Submit</button>
            </form>
        )
        console.log("abouts : ", this.props.abouts)
        let aboutList = (
            <table className={classes.AboutList}>
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Data</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    {this.props.aboutData.map(about => (
                        <tr key={about.aboutId}>
                            <td>{about.aboutId}</td>
                            <td>{about.aboutName}</td>
                            <td>{about.aboutTitle}</td>
                            <td>{about.aboutContent}</td>
                            <td></td>
                            <td>
                                <button className={classes.AboutListBtn} onClick={() => this.props.onEditAbout(about.aboutId)}><AiIcons.AiFillEdit /></button>
                                <button className={classes.AboutListBtn} onClick={() => this.props.onDeleteAbout(about.aboutId)}><AiIcons.AiFillDelete /> </button></td>
                        </tr>
                    ))}

                </tbody>
            </table>
        )

        return (
            <div className={classes.ShowCase}>

                <div className={classes.AboutContainer}>
                    <br />
                    <Modal show={this.props.editable} modalClosed={this.props.onCloseModal}>
                        <EditAboutModal editAbouts={this.props.editAbouts} />
                    </Modal>
                    {form}
                    {aboutList}
                    <br />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    abouts: state.about.addAbout,
    aboutData: state.about.abouts,
    editAbouts: state.about.editAbouts,
    editable: state.about.editable
})

const mapDispatchToProps = (dispatch) => ({
    onAddAbout: (formData) => dispatch(action.addAbout(formData)),
    onUpdateEditAboutState: (updateEditAbout) => dispatch(action.updateStateEditAbout(updateEditAbout)),
    onFetchAbout: () => dispatch(action.fetchAbout()),
    onEditAbout: (id) => dispatch(action.editAbout(id)),
    onDeleteAbout: (id) => dispatch(action.deleteAbout(id)),
    onCloseModal: () => dispatch(action.closeEditAboutModal())
})


export default connect(mapStateToProps, mapDispatchToProps)(EditAbout)