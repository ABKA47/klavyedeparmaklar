import React, { useState } from 'react'
import { NavLink } from 'react-router-dom';
import * as FaIcons from 'react-icons/fa';
import { GiExitDoor } from 'react-icons/gi';
import AuthService from '../../Auth/Services/Auth.service';
import classes from './DashboardSidebar.css';

const logout = () => {

    AuthService.logout();
    window.location.reload();

}

const DashboardSidebar = () => {
    const [openSubMenuItems, setSubMenu] = useState(false);
    const [setDisplay, onSetDisplay] = useState(false)
    return (
        <nav className={classes.DashboardNavbar}>
            {JSON.parse(localStorage.getItem('user')).roles[0] === "ROLE_THEBOSS" || JSON.parse(localStorage.getItem('user')).roles[0] === "ROLE_DEVELOPER" ?
                <div className={classes.DashboardMenu} onMouseEnter={() => onSetDisplay(!setDisplay)} onMouseLeave={() => onSetDisplay(!setDisplay)}>

                    <ul>
                        <li><NavLink to="/dashboard" ><FaIcons.FaHome /><span>Home</span></NavLink></li>
                        <li><NavLink to="#" onClick={() => setSubMenu(!openSubMenuItems)}><FaIcons.FaEdit /><span>Edit Pages</span></NavLink>
                            <ul style={{ top: openSubMenuItems ? '0' : '-100%', display: (openSubMenuItems && setDisplay) ? 'block' : 'none', transition: '0.8s ease-in all', border: '1px var(--bizim-parlak-yesil)' }}>
                                <li style={{ paddingLeft: '5px', margin: '10px 0', opacity: setDisplay ? '1' : '0' }}><NavLink to="/dashboard/editproject" ><span>Projects</span></NavLink></li>
                                <li style={{ paddingLeft: '5px', margin: '10px 0', opacity: setDisplay ? '1' : '0' }}><NavLink to="/dashboard/editabout"><span>About</span></NavLink></li>
                            </ul>
                        </li>
                        <li><NavLink to="/dashboard/register"><FaIcons.FaUserPlus /><span>Register</span></NavLink></li>
                        <li><NavLink to="/logout" href="/home" onClick={logout}><GiExitDoor /><span>Logout</span></NavLink></li>
                    </ul>
                </div> :
                <div className={classes.DashboardMenu}>
                    <ul>
                        <li><NavLink to="/logout" href="/home" onClick={logout}><GiExitDoor /><span>Logout</span></NavLink></li>
                    </ul>
                </div>
            }
        </nav>

    )
}

export default DashboardSidebar;