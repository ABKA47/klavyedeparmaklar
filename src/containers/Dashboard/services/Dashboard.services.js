import axios from '../../../axios';

class DashboardService {
    editAbout(abouts) {
        let formData = new FormData();

        formData.append("aboutId", abouts.aboutId);
        formData.append("aboutName", abouts.aboutName);
        formData.append("aboutTitle", abouts.aboutTitle);
        formData.append("aboutContent", abouts.aboutContent);
        formData.append("aboutData", abouts.aboutData);

        return axios.post("about/editabout", formData)
    }
    editProject(projects) {
        let formData = new FormData();

        formData.append("projectId", projects.projectId);
        formData.append("projectName", projects.projectName);
        formData.append("projectContent", projects.projectContent);
        formData.append("projectData", projects.projectData);

        return axios.post("project/editproject", formData)
    }
}
export default new DashboardService()