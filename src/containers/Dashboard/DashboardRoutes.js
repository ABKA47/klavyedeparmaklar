import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import EditProject from './EditProject/EditProject'
import EditAbout from './EditAbout/EditAbout';
import Register from '../Auth/Register/Register';
import Home from './Home/Home';

import classes from './DashboardRoutes.css'
class DashboardRoutes extends Component {
    render() {
        return (
            <div className={classes.DashboardRoutes}>
                <Switch>
                    <Route exact path="/dashboard" component={Home} />
                    <Route exact path="/dashboard/register" component={Register} />
                    <Route exact path="/dashboard/editproject" component={EditProject} />
                    <Route exact path="/dashboard/editabout" component={EditAbout} />
                </Switch>
            </div>
        )
    }
}

export default DashboardRoutes;