import React, { Component } from 'react'
import { connect } from 'react-redux';

import * as action from '../../../store/actions/index';
import Input from '../../../components/UI/Input/Input';
import Modal from '../../../components/UI/Modal/Modal';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import EditProjectModal from './EditProjectModal/EditProjectModal';
import classes from './EditProject.css'
import Pagination from '../../../components/UI/Pagination/Pagination';
import * as AiIcons from 'react-icons/ai'

class EditProject extends Component {
    componentDidMount() {
        this.props.onFetchProjects();
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedProjects = { ...this.props.addProjects };
        const updatedState = { ...updatedProjects[inputIdentifier] };
        updatedState.value = event.target.value;
        updatedState.fileValue = event.target.files
        updatedProjects[inputIdentifier] = updatedState
        this.props.onUpdateEditProjects(updatedProjects);
    }
    editorChangedHandler = (event, inputIdentifier, editor) => {
        const updatedProjects = { ...this.props.addProjects };
        const updatedState = { ...updatedProjects[inputIdentifier] };
        updatedState.data = editor.getData();
        updatedProjects[inputIdentifier] = updatedState
        this.props.onUpdateEditProjects(updatedProjects);
    }
    submitForm = (event) => {
        event.preventDefault();
        const projectsList = {
            projectName: this.props.addProjects.projectName.value,
            projectContent: this.props.addProjects.projectContent.data,
            projectData: this.props.addProjects.projectData.fileValue[0]
        }
        let formData = new FormData();

        formData.append("projectName", projectsList.projectName)
        formData.append("projectContent", projectsList.projectContent)
        formData.append("projectData", projectsList.projectData)

        this.props.onEditProjects(formData);
    }
    render() {

        const indexOfLastContent = this.props.currentPage * this.props.contentPerPage
        const indexOfFirstContent = indexOfLastContent - this.props.contentPerPage
        const currentContent = this.props.projects.slice(indexOfFirstContent, indexOfLastContent)

        const paginate = (pageNumber) => this.props.onSetCurrentPage(pageNumber)

        let projectsArray = [];
        for (let key in this.props.addProjects) {
            projectsArray.push({
                id: key,
                config: this.props.addProjects[key]
            })
        }

        let form = (
            <form onSubmit={this.submitForm} className={classes.Card} >
                <h3>Edit Project Page</h3>
                {projectsArray.map(projectsElement => (
                    <div className={classes.EditProject}>
                        <Input
                            key={projectsElement.id}
                            elementType={projectsElement.config.type}
                            elementConfig={projectsElement.config.elementConfig}
                            value={projectsElement.config.value}
                            data={projectsElement.config.data}
                            placeholder={projectsElement.config.placeholder}
                            editor={ClassicEditor}
                            changed={(event) => this.inputChangedHandler(event, projectsElement.id)}
                            editorChanged={(event, editor) => this.editorChangedHandler(event, projectsElement.id, editor)} />
                    </div>
                ))}
                <button className={classes.ProjectBtn} >SUBMIT</button>
            </form>
        )
        let projectList = (
            <table className={classes.ProjectList}>
                <thead>
                    <th>Project ID</th>
                    <th>Project Name</th>
                    <th>Project Content</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    {currentContent.map(project => (
                        <tr key={project.projectId}>
                            <td>{project.projectId}</td>
                            <td>{project.projectName}</td>
                            <td dangerouslySetInnerHTML={{ __html: project.projectContent }}></td>
                            <td>
                                <button className={classes.ProjectListBtn} onClick={() => this.props.onEditProject(project.projectId)}><AiIcons.AiFillEdit /></button>
                                <button className={classes.ProjectListBtn} onClick={() => this.props.onDeleteProject(project.projectId)}><AiIcons.AiFillDelete /></button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        )
        return (
            <div className={classes.ShowCase}>

                <div className={classes.ProjectContainer}>
                    <br />
                    <Modal show={this.props.editable} modalClosed={this.props.onCloseModal}>
                        <EditProjectModal project={this.props.projectList} />
                    </Modal>
                    {form}
                    {projectList}
                    <Pagination
                        contentPerPage={this.props.contentPerPage}
                        totalContent={this.props.projects.length}
                        paginate={paginate}
                    />
                    <br />
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        addProjects: state.projects.addProjects,
        projects: state.projects.projects,
        projectList: state.projects.projectList,
        editable: state.projects.editable,
        currentPage: state.projects.currentPage,
        contentPerPage: state.projects.contentPerPage
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onUpdateEditProjects: (projectsData) => dispatch(action.updateStateEditProject(projectsData)),
        onEditProjects: (project) => dispatch(action.addProjects(project)),
        onFetchProjects: () => dispatch(action.fetchProjects()),
        onEditProject: (id) => dispatch(action.editProject(id)),
        onDeleteProject: (id) => dispatch(action.deleteProject(id)),
        onCloseModal: () => dispatch(action.closeEditProjectModal()),
        onSetCurrentPage: (pageNumber) => dispatch(action.SetCurrentPage(pageNumber))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProject);