import React, { Component } from 'react'

import { connect } from 'react-redux'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Input from '../../../../components/UI/Input/Input'
import classes from './EditProjectModal.css'
import DashboardService from '../../services/Dashboard.services'
import * as action from '../../../../store/actions/index'

class EditProjectModal extends Component {

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedAbout = { ...this.props.editProjects };
        const updatedState = { ...updatedAbout[inputIdentifier] };
        updatedState.value = event.target.value;
        updatedState.fileValue = event.target.files
        updatedAbout[inputIdentifier] = updatedState
        this.props.onUpdateEditProjectState(updatedAbout);
    }
    editorChangedHandler = (event, inputIdentifier, editor) => {
        const updatedAbout = { ...this.props.editProjects };
        const updatedState = { ...updatedAbout[inputIdentifier] };
        updatedState.data = editor.getData();
        updatedAbout[inputIdentifier] = updatedState
        this.props.onUpdateEditProjectState(updatedAbout);
    }
    formSubmit = (event) => {
        event.preventDefault()
        // const editProjectsList = {
        //     aboutName: this.props.editProjects.aboutName.value,
        //     aboutTitle: this.props.editProjects.aboutTitle.value,
        //     aboutContent: this.props.editProjects.aboutContent.data,
        // }
        // let formData = new FormData();

        // formData.append("aboutName", editProjectsList.aboutName.aboutName)
        // formData.append("aboutTitle", editProjectsList.aboutTitle.aboutTitle)
        // formData.append("aboutContent", editProjectsList.aboutContent.aboutContent)
        const editProjects = {
            
            projectId: this.props.projectList[0].projectId,
            projectName: this.props.editProjects.projectName.value,
            projectContent: this.props.editProjects.projectContent.data,
            projectData: this.props.editProjects.projectData.fileValue[0]
        }

        const editProjectsList = {
            editProjects: editProjects
        }
        DashboardService.editProject(editProjectsList.editProjects)
    }
    render() {
        const projectList = this.props.project.map((user, key) => {
            let editProjectArray = []

            for (let key in this.props.editProjects) {
                editProjectArray.push({
                    id: key,
                    config: this.props.editProjects[key]
                })
            }

            return (
                <div className={classes.RegisterContainer} key={key}>
                    <br />
                    <form onSubmit={this.formSubmit} className={classes.EditRegisterForm, classes.Card}>
                        <h3>EDIT PROJECT CARD</h3>
                        {editProjectArray.map(inputElement => (
                            <div className={classes.EditRegister} key={inputElement.id}>
                                <Input
                                    key={inputElement.id}
                                    elementType={inputElement.config.type}
                                    elementConfig={inputElement.config.elementConfig}
                                    placeholder={inputElement.config.elementConfig.placeholder}
                                    value={inputElement.config.value}
                                    data={inputElement.config.data}
                                    invalid={!inputElement.config.validation}
                                    shouldValidate={inputElement.config.validation}
                                    touched={inputElement.config.touched}
                                    editor={ClassicEditor}
                                    changed={(event) => this.inputChangedHandler(event, inputElement.id)}
                                    editorChanged={(event, editor) => this.editorChangedHandler(event, inputElement.id, editor)}
                                />
                            </div>
                        ))
                        }
                        <button className={classes.RegisterBtn}>UPDATE PROJECT CARD</button>
                    </form>
                    <br />
                </div>

            )
        })
        return (
            <div>
                {projectList}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        editProjects: state.projects.editProjects,
        projectList: state.projects.projectList
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onUpdateEditProjectState: (updatedEditProjectState) => dispatch(action.updateStateEditProjectModal(updatedEditProjectState))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProjectModal)