import React, { Component } from 'react'
import classes from './ProjectsModal.css'
class ProjectsModal extends Component {
    render() {
        console.log("projectsModalList : ", this.props.projectsModalList)
        let project = this.props.projectsModalList.map(list => (
            <div key={list.projectId} className={classes.ProjectsModal}>
                <h2>{list.projectName}</h2>
                <p dangerouslySetInnerHTML={{ __html: list.projectContent }}></p>
            </div>
        ))
        return (
            <div>
                {project}
            </div>
        )
    }
}

export default ProjectsModal;