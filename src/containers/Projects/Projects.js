import React, { Component } from 'react'
import { connect } from 'react-redux';

import classes from './Projects.css';
import * as action from '../../store/actions/index';
import Modal from '../../components/UI/Modal/Modal';
import ProjectsModal from './ProjectsModal/ProjectsModal';
class Projects extends Component {
    componentDidMount() {
        this.props.onFetchProjects();
    }
    render() {
        return (
            <div className={classes.Projects}>
                <Modal show={this.props.show} modalClosed={this.props.onCloseProjectsModal}>
                    <ProjectsModal projectsModalList={this.props.projectsModalList} />
                </Modal>
                <div className={classes.Container, classes.Flex}>
                    {this.props.projects.map(project => (
                        <div className={classes.ProjectsCard} key={project.projectId} onClick={() => this.props.onOpenProjectsModal(project.projectId)}>
                            <img className={classes.ProjectsImg} src={`../images/projects/${project.projectFileName}.${project.projectFileType}`} />
                            <div className={classes.ProjectNameCard}>
                                <h2>{project.projectName}</h2>
                            </div>
                        </div>
                    ))}

                </div>

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        projects: state.projects.projects,
        projectsModalList: state.projects.projectsModalList,
        show: state.projects.show
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchProjects: () => dispatch(action.fetchProjects()),
        onOpenProjectsModal: (projectsId) => dispatch(action.openProjectsModal(projectsId)),
        onCloseProjectsModal: () => dispatch(action.closeProjectsModal())

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Projects);