import React, { Component } from 'react'
import { connect } from 'react-redux';
import Input from '../../../components/UI/Input/Input';
import * as action from '../../../store/actions/index';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './Login.css';

class Login extends Component {
    inputChangedHandler = (event, inputIdentifier) => {
        const updatedLogin = { ...this.props.login };
        const updatedState = { ...updatedLogin[inputIdentifier] };
        updatedState.value = event.target.value;
        updatedLogin[inputIdentifier] = updatedState
        this.props.onLoginState(updatedLogin);
    }
    submitForm = (event) => {
        event.preventDefault();
        const loginData = {};
        for (let id in this.props.login) {
            loginData[id] = this.props.login[id].value;
        }

        const loginList = {
            loginData: loginData
        }

        this.props.onLogin(loginList.loginData.password, loginList.loginData.userName)
    }
    render() {
        let loginArray = [];
        for (let key in this.props.login) {
            loginArray.push({
                id: key,
                config: this.props.login[key]
            })
        }

        let form = (
            <form onSubmit={this.submitForm} className={classes.Card}>
                {loginArray.map(formElement => (
                    <div className={classes.FormElement}>
                        <Input
                            key={formElement.id}
                            elementType={formElement.config.type}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)} />

                    </div>
                ))}
                <button className={classes.LoginBtn}>SUBMIT</button>
            </form>
        )
        if (this.props.loading) {
            form = <Spinner />
        }

        return (
            <div className={classes.LoginContainer}>
                {form}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        login: state.auth.login,
        loading: state.auth.loading
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onLoginState: (updatedLogin) => dispatch(action.updateLoginState(updatedLogin)),
        onLogin: (userName, userPassword) => dispatch(action.authenticationLogin(userName, userPassword))

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);