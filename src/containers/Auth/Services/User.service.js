import axios from '../../../axios'

const API_URL_ABOUT = 'about/addabout'
const API_URL_PROJECTS = 'projects/addproject'
const API_URL_FETCHUSERS = 'auth/fetchusers'

class UserService {

  getAllUsers() {
    return axios.get(API_URL_FETCHUSERS)
  }
}

export default new UserService();
