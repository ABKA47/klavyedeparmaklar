import axios from '../../../axios';

class AuthService {
    login(userPassword, userName) {
        return axios.post("auth/signin", {
            userPassword,
            userName
        })
    }
    logout() {
        localStorage.removeItem("user");
    }
    register(userName, userPassword, role) {
        return axios.post("auth/signup", {
            userName,
            userPassword,
            role
        });
    }
    editUser(userId, userName, userPassword, role) {
        return axios.post("auth/edituser", {
            userId,
            userName,
            userPassword,
            role
        })
    }
}

export default new AuthService();