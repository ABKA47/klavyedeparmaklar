import React, { Component } from 'react'

import { connect } from 'react-redux'
import * as action from '../../../store/actions/index';
import Input from '../../../components/UI/Input/Input'
import AuthService from '../Services/Auth.service';
import EditUser from '../../Dashboard/EditUser/EditUser'
import Modal from '../../../components/UI/Modal/Modal'
import classes from './Register.css'
import * as AiIcons from 'react-icons/ai'

class Register extends Component {

    componentDidMount() {
        this.props.onFetchUsers()
    }
    checkValidity(value, rules) {
        let isValid = true
        if (!rules) {
            return true;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.isMail) {
            const pattern = /^.+@.+$/;
            isValid = pattern.test(value) && isValid
        }
        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedRegister = { ...this.props.register }
        const updatedState = { ...updatedRegister[inputIdentifier] }

        updatedState.value = event.target.value
        updatedState.valid = this.checkValidity(updatedState.value, updatedState.validation)
        updatedState.touched = true
        updatedRegister[inputIdentifier] = updatedState
        let formIsValid = true
        for (let inputIdentifier in updatedRegister) {
            formIsValid = updatedRegister[inputIdentifier].valid && formIsValid
        }
        this.props.onRegisterState(updatedRegister, formIsValid)
    }

    formSubmit = (event) => {
        event.preventDefault()
        const registerData = {}
        for (let id in this.props.register) {
            registerData[id] = this.props.register[id].value
        }
        const registerList = {
            registerData: registerData
        }
        console.log("register list : ", registerList)
        AuthService.register(registerList.registerData.userName, registerList.registerData.password, registerList.registerData.role).then(response => {
            console.log("messages:", response.data.message)
            window.location.reload()
        }, error => {
            console.log("Error: ", error.response)
        })
    }

    render() {
        var byte;
        let inputArray = []
        for (let key in this.props.register) {
            inputArray.push({
                id: key,
                config: this.props.register[key]
            })
        }
        let form = (
            <form onSubmit={this.formSubmit} className={classes.EditRegisterForm, classes.Card}>
                <h3>Register</h3>
                {inputArray.map(inputElement => (
                    <div className={classes.EditRegister}>
                        <Input
                            key={inputElement.id}
                            elementType={inputElement.config.type}
                            elementConfig={inputElement.config.elementConfig}
                            placeholder={inputElement.config.elementConfig.placeholder}
                            value={inputElement.config.value}
                            invalid={!inputElement.config.validation}
                            shouldValidate={inputElement.config.validation}
                            touched={inputElement.config.touched}
                            changed={(event) => this.inputChangedHandler(event, inputElement.id)}
                        />
                    </div>
                ))
                }
                <button disabled={!this.props.formIsValid} className={classes.RegisterBtn}>Register new crew!</button>
            </form>
        )
        let userList = (
            <table className={classes.RegisterList}>
                <thead>
                    <th>User ID</th>
                    <th>User Name</th>
                    <th>User Role</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    {this.props.users.map(user => (
                        <tr>
                            <td>{user.userId}</td>
                            <td>{user.userName}</td>
                            <td>{user.roles[0].name}</td>
                            <td>
                                <button className={classes.RegisterListBtn} onClick={() => this.props.onEditUser(user.userId)}><AiIcons.AiFillEdit /></button>
                                <button className={classes.RegisterListBtn} onClick={() => this.props.onDeleteUser(user.userId)}><AiIcons.AiFillDelete /></button>
                            </td>
                        </tr>
                    ))}

                </tbody>
            </table>

        )
        console.log("editUsers: ", this.props.editUsers)
        return (
            <div>
                <div className={classes.RegisterContainer}>
                    <br />
                    {form}
                    <br />
                    {userList}
                    <Modal show={this.props.editTable} modalClosed={this.props.onCloseModal}>
                        <EditUser user={this.props.editUserList} />
                    </Modal>

                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        register: state.auth.register,
        formIsValid: state.auth.registerFormIsValid,
        editTable: state.auth.editTable,
        users: state.auth.users,
        editUserList: state.auth.editUserList
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRegisterState: (register, formIsValid) => dispatch(action.updatedRegisterState(register, formIsValid)),
        onFetchUsers: () => dispatch(action.fetchUsersData()),
        onEditUser: (id) => dispatch(action.editUser(id)),
        onCloseModal: () => dispatch(action.closeModal()),
        onDeleteUser: (id) => dispatch(action.deleteUser(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)