import React, { Component } from 'react'

import classes from './Contact.css';
import Input from '../../components/UI/Input/Input';
import { connect } from 'react-redux';
import * as action from '../../store/actions/index';

//Google Map
import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps'

class Contact extends Component {
    checkValidity(value, rules) {
        let isValid = true
        if (!rules) {
            return true;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.isLetter) {
            const pattern = /^[a-zA-Z\s]*$/;
            isValid = pattern.test(value) && isValid
        }
        if (rules.isMail) {
            let pattern = /^.+@.+$/
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedContact = { ...this.props.contact }
        const updatedContactState = { ...updatedContact[inputIdentifier] }
        updatedContactState.value = event.target.value
        updatedContactState.valid = this.checkValidity(updatedContactState.value, updatedContactState.validation)
        updatedContactState.touched = true;
        updatedContact[inputIdentifier] = updatedContactState
        let formIsValid = true;
        for (let inputIdentifier in this.props.contact) {
            formIsValid = updatedContact[inputIdentifier].valid && formIsValid
        }
        this.props.onUpdateContactState(updatedContact, formIsValid)
    }

    submitForm = (event) => {
        event.preventDefault();
        const contactData = {};
        for (let id in this.props.contact) {
            contactData[id] = this.props.contact[id].value
        }

        const contactList = {
            contactData: contactData
        }

        console.log("Contact Data :", contactList.contactData)
    }

    render() {

        let formElementArray = [];
        for (let key in this.props.contact) {
            formElementArray.push(
                {
                    id: key,
                    config: this.props.contact[key]
                }
            )
        }

        let form = (
            <form onSubmit={this.submitForm} className={classes.Card} >
                {formElementArray.map(formElement => (
                    <div key={formElement.id} className={classes.FormElement}>
                        <Input
                            key={formElement.id}
                            elementType={formElement.config.type}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            invalid={!formElement.config.valid}
                            shouldValidate={formElement.config.validation}
                            touched={formElement.config.touched}
                            placeholder={formElement.config.displayValue}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                    </div>
                ))}
                <button className={classes.Button} disabled={!this.props.formIsValid}>SUBMIT</button>
            </form >
        )

        let mapFunctions = () => (
            <GoogleMap
                defaultZoom={10}
                defaultCenter={{ lat: 51.515429518561675, lng: -0.13151528483353528 }}
            >
                <Marker
                    position={{ lat: 51.51684330708043, lng: -0.22628565734476272 }}
                />
            </GoogleMap>
        )

        const WrappedMap = withScriptjs(withGoogleMap(mapFunctions))

        return (
            <div>
                <div className={classes.Showcase}>
                    <div className={classes.Container, classes.Grid}>
                        <div className={classes.Info}>
                            <h1>Feel to free contact via Mail</h1>
                            <br />
                            <h3>info@klavyedeparmaklar.com</h3>
                        </div>
                        <div className={classes.Form}>
                            {form}
                        </div>
                    </div>
                </div>
                <div className={classes.GoogleMap, classes.GoogleShow}>
                        <WrappedMap
                            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKY6At88OLhYfvSRrlIcllYU5KXKtOwoE&v=3.exp&libraries=geometry,drawing,places"
                            loadingElement={<div style={{ height: "100%" }} />}
                            containerElement={<div style={{ height: "100%" }} />}
                            mapElement={<div style={{ height: "100%" }} />}
                        />
                        {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d774.54829570055!2d-0.2259044342096143!3d51.51642403467004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761028524ee041%3A0x7e590e5ba9c047a5!2sShinfield%20St%2C%20London%20W12%200HN!5e0!3m2!1str!2suk!4v1613672469550!5m2!1str!2suk" 
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> */}
                    </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        contact: state.contact.contact,
        formIsValid: state.contact.formIsValid
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateContactState: (updatedContact, formIsValid) => dispatch(action.updateContactState(updatedContact, formIsValid))


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Contact);