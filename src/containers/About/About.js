import React, { Component } from 'react'

//Redux
import { connect } from 'react-redux'
import * as action from '../../store/actions/index'

//Icons & CSS
import * as FaIcons from 'react-icons/fa'
import classes from './About.css'

class About extends Component {
    componentDidMount() {
        this.props.onfetchAbout();
    }

    render() {
        return (
            <div className={classes.About}>
                <div className={classes.Grid}>
                    <div className={classes.AboutDescription}>
                        <h1>KP TEAM</h1>
                        <p>
                            We are truly passionate engineers and entrepreneurs whose mission is to deliver scalable and efficient software solutions.
                            That is why we genuinely care about high quality code, attractive design, accessibility and customer experience.
                        </p>
                    </div>
                    <div>
                        <h2>Image</h2>
                    </div>
                </div>
                <h2>Our Crew...</h2>
                <div className={classes.Container, classes.Flex}>
                    {this.props.abouts.map(about => (
                            <div className={classes.AboutCard} key={about.aboutId}>
                                <div className={classes.AboutImg}><img src={`../images/Abouts/${about.aboutFileName}.${about.aboutFileType}`} /></div>
                                <h2>{about.aboutName}<a href={about.aboutContent} target="__Blank"> <FaIcons.FaLinkedin /></a></h2>
                                <span>{about.aboutTitle}</span>
                            </div>
                    ))}
                </div>
                <section className={classes.Languages}>
                    <h2>We develop via these languages</h2>
                    <div className={classes.Container, classes.Flex}>
                        <div className={classes.Card}>
                            <h4>ReactJS</h4>
                            <img src="../images/Logos/react82-80.png" />
                        </div>
                        <div className={classes.Card}>
                            <h4>Java</h4>
                            <img src="../images/Logos/java82-80.png" />
                        </div>
                        <div className={classes.Card}>
                            <h4>Spring Boot</h4>
                            <img src="../images/Logos/spring82-80.png" />
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    abouts: state.about.abouts
})

const mapDispatchToProps = (dispatch) => ({
    onfetchAbout: () => dispatch(action.fetchAbout()),
})


export default connect(mapStateToProps, mapDispatchToProps)(About)