# KLAVYEDEPARMAKALAR.COM

### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
<br>

##  This is a web application written using
-   ReactJS in Front-End
-   Spring Boot Framework in Back-End
-   SSMS for SQL

<br>
![Contact](/src/assets/Contact.JPG)
<br>

## You will find them in this project :exclamation:
-   Connecting to Google Maps API 
-   Fetch data from our Back-End 
-   Connecting to SSMS for SQL Database
-   React-Icon 

### [Backend Codes](https://gitlab.com/ABKA47/kpbackend) :sparkles: